import Vue from 'vue'
import Router from 'vue-router'
import ListPage from '@/components/ListPage'
import DetailPage from '@/components/detailPage'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'ListPage',
    component: ListPage
  }, {
    path: '/DetailPage',
    name: 'DetailPage',
    component: DetailPage
  }]
})
